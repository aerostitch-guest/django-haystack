django-haystack (2.8.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Sat, 24 Mar 2018 19:40:40 +0100

django-haystack (2.7.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Michael Fladischer ]
  * New upstream release.
  * Always use pristine-tar.
  * Update license definition for SOLR auxiliary files.
  * Remove lintian override for missing GPG signature check on source
    tarball to serve as a reminder to improve on this.
  * Bump Standards-Version to 4.1.3.
  * Bump debhelper compatibility and version to 11.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Enable autopkgtest-pkg-python testsuite.
  * Build documentation in override_dh_sphinxdoc.

 -- Michael Fladischer <fladi@debian.org>  Wed, 14 Feb 2018 19:15:48 +0100

django-haystack (2.6.1-1) unstable; urgency=low

  * Upload to unstable.
  * Bump Standards-Version to 4.0.0.

 -- Michael Fladischer <fladi@debian.org>  Mon, 19 Jun 2017 21:26:15 +0200

django-haystack (2.6.1-1~exp1) experimental; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Wed, 31 May 2017 11:26:09 +0200

django-haystack (2.6.0-1~exp1) experimental; urgency=low

  * New upstream release.
  * Add .eggs/README.txt to d/clean to allow two builds in a row.
  * Add python(3)-setuptools-scm to Build-Depends as upstream started
    using it in their setup.py.
  * Remove unused lintian overrides.

 -- Michael Fladischer <fladi@debian.org>  Tue, 21 Mar 2017 12:40:47 +0100

django-haystack (2.5.1-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Tue, 03 Jan 2017 15:09:30 +0100

django-haystack (2.5.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Michael Fladischer ]
  * New upstream release (Closes: #828641).
  * Bump Standards-Version to 3.9.8.
  * Use github tarballs in d/watch.
  * Use python3-sphinx to build documentation.
  * Use https:// for copyright-format 1.0 URL.

 -- Michael Fladischer <fladi@debian.org>  Sun, 21 Aug 2016 12:35:16 +0200

django-haystack (2.4.1-1) unstable; urgency=low

  [ Michael Fladischer ]
  * New upstream release.

  [ SVN-Git Migration ]
  * git-dpm config
  * Update Vcs fields for git migration

 -- Michael Fladischer <fladi@debian.org>  Wed, 04 Nov 2015 13:11:02 +0100

django-haystack (2.4.0-2) unstable; urgency=low

  * Switch buildsystem to pybuild.
  * Add Python3 support through a separate package.
  * Add lintian override for missing upstream changelog.

 -- Michael Fladischer <fladi@debian.org>  Tue, 07 Jul 2015 22:12:20 +0200

django-haystack (2.4.0-1) unstable; urgency=low

  * New upstream release.
  * Remove files from d/copyright which are no longer shipped by
    upstream.
  * Use pypi.debian.net service for uscan.
  * Change my email address to fladi@debian.org.

 -- Michael Fladischer <fladi@debian.org>  Tue, 07 Jul 2015 16:18:03 +0200

django-haystack (2.3.1-1) unstable; urgency=medium

  * New upstream release (Closes: #755599).
  * Bump Standards-Version to 3.9.6.
  * Disable tests as they require a live SOLR and elasticsearch server.
  * Change file names for solr configuration files in d/copyright.
  * Make pysolr require at least version 3.2.0.
  * Add python-elasticsearch to Suggests.
  * Drop packages required by tests from Build-Depends:
    + python-django
    + python-httplib2
    + python-mock
    + python-pysolr
    + python-whoosh
  * Drop python-xapian from suggests as the xapian backend is not
    included.
  * Add django_haystack.egg-info/requires.txt to d/clean.
  * Remove empty lines at EOF for d/clean and d/rules.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Mon, 20 Oct 2014 14:18:24 +0200

django-haystack (2.1.0-1) unstable; urgency=low

  * Initial release (Closes: #563311).

 -- Michael Fladischer <FladischerMichael@fladi.at>  Thu, 13 Mar 2014 19:11:15 +0100
